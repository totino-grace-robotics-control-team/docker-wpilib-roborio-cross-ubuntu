These Ubuntu images have the necessary cross-build tools (from wpilib/roborio-toolchain) and native tools, as well as the correct JDK installed for building applications targeted at the RoboRIO.

Installed packages:
 - OpenJDK
 - build-essential, cmake, g++, gcc, gdb, make
 - roboRIO toolchain
 - libopencv-dev, python-all-dev
 - curl, unzip, wget, zip
